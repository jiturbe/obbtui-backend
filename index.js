'use strict';

var app = require('./app'),
  infoApp = require('./package.json'),
  config = require('./config'),
  log = require('./base/utils/logConsola');

log.log('Esperando 3 segundos antes de iniciar servidor');

setTimeout(function() {
  app.listen(config.app.port, function() {
    log.log(`${infoApp.name} (v ${infoApp.version}) funcionando en http://localhost:${config.app.port}`);
  });
}, 3000);