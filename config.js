'use strict';

var config = {
  app: {
    port: 8080
  }, 
  services: [
    { name: 'acceso', url: 'http://localhost:2000/acceso' },
    { name: 'novedades', url: 'http://localhost:2000/novedades' },
    { name: 'notificaciones', url: 'http://localhost:2000/notificaciones' }
  ]
};

module.exports = config;